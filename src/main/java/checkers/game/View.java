package checkers.game;

import checkers.engine.model.board.Board;
import checkers.engine.model.board.Piece;
import checkers.engine.util.LogContext;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.util.stream.IntStream;

/**
 * <p>
 *     Abstraction for the UI of the checkers game.
 * </p>
 * @author Prakash on 8/29/17.
 */
public class View {

    private static final LogContext LOGGER = new LogContext(View.class);

    private JFrame _frame;
    private JPanel _container;
    private JPanel _statusPanel;
    private JPanel _panel;
    private JButton _aboutButton;
    private JButton _connectButton;
    private JButton _submitMoveButton;
    private JButton _clearButton;
    private JButton _helpButton;
    private JLabel _statusLabel;
    private JButton[][] _buttonArray;

    /**
     * Default constructor
     */
    public View()
    {
        _buttonArray = new JButton[8][8];
        _frame = new JFrame("Checkers Game");
        _panel = new JPanel();
        _container = new JPanel();
        _statusPanel = new JPanel();
        _aboutButton = new JButton("About");
        _connectButton = new JButton("Connect");
        _submitMoveButton = new JButton("Submit Move");
        _clearButton = new JButton("Clear Move");
        _helpButton = new JButton("Help");
        _statusLabel = new JLabel("");

        //set up the board
        IntStream.range(0, 64)
                .forEach(x -> {
                    int i = x / 8;
                    int j = x % 8;

                    _buttonArray[i][j] = new JButton();
                    _buttonArray[i][j].setPreferredSize(new Dimension(80,80));
                    _buttonArray[i][j].setFocusPainted(false);
                    _buttonArray[i][j].setForeground(Color.BLACK);
                    if (((i-j)%2) == 0)
                    {
                        _buttonArray[i][j].setBackground(Color.BLACK);
                    }
                    else
                    {
                        _buttonArray[i][j].setBackground(Color.WHITE);
                    }
                    _buttonArray[i][j].addActionListener(e -> {
                        Point buttonIndex = findButtonLocation((JButton) e.getSource());
                        Client.gameButtonClicked(buttonIndex);
                    });
                    _panel.add(_buttonArray[i][j]);
                });

        //set the top part of the UI
        _container.setLayout(new BoxLayout(_container, BoxLayout.Y_AXIS));
        _connectButton.setFocusPainted(false);
        _aboutButton.setFocusPainted(false);

        _aboutButton.addActionListener(e -> Client.aboutButtonClicked());
        _connectButton.addActionListener(e -> Client.connectButtonClicked());
        _clearButton.addActionListener(e -> Client.clearButtonClicked());
        _submitMoveButton.addActionListener(e -> Client.submitButtonClicked());
        _helpButton.addActionListener(e -> Client.helpButtonClicked());

        _statusPanel.add(_connectButton);
        _statusPanel.add(_statusLabel);
        _statusPanel.add(_submitMoveButton);
        _statusPanel.add(_clearButton);
        _statusPanel.add(_aboutButton);
        _statusPanel.add(_helpButton);
        _container.add(_statusPanel);

        _panel.setLayout(new GridLayout(8, 8));
        _container.add(_panel);
        _frame.add(_container);
        _frame.pack();
        _frame.setLocationRelativeTo(null);
        _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _frame.setVisible(true);

        _frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                Client.sendForfeit();
            }
        });
    }

    /**
     * Find which button was clicked and return the index of it as a Point
     * @param button {@link JButton}
     * @return {@link Point}
     */
    protected Point findButtonLocation(JButton button)
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (_buttonArray[i][j] == button)
                {
                    return new Point(i,j);
                }
            }
        }
        return null;
    }

    /**
     * Gets the position background color for the give {@link Point}
     * @param p {@link Point}
     * @return {@link Color}
     */
    protected Color getPositionBGColor(Point p){
        return _buttonArray[p.x][p.y].getBackground();
    }

    /**
     * Sets the position background color for the given {@link Point}
     * @param p {@link Point}
     * @param c {@link Color}
     */
    protected void setPositionBGColor(Point p, Color c){
        _buttonArray[p.x][p.y].setBackground(c);
    }

    /**
     * Updates the UI with the latest board
     * @param board {@link Board} instance
     */
    protected void updateView(Board board)
    {
        IntStream.range(0, 64)
                .forEach(x -> {
                    int i = x / 8;
                    int j = x % 8;

                    Piece currentChar = board.getState()[i][j];
                    switch (currentChar) {
                        case EMPTY:
                            _buttonArray[i][j].setText("");
                            _buttonArray[i][j].setIcon(null);
                            break;
                        case RED:
                            try {
                                Image img = ImageIO.read(getClass().getResource("/images/red piece.jpg"));
                                img = img.getScaledInstance(50,50,Image.SCALE_DEFAULT);
                                _buttonArray[i][j].setIcon(new ImageIcon(img));
                            } catch (Exception ex) {
                                LOGGER.withContext("Method", "updateview")
                                        .withContext("Case", "red").error(ex);
                            }
                            break;
                        case RED_KING:
                            try {
                                Image img = ImageIO.read(getClass().getResource("/images/redking piece.jpg"));
                                img = img.getScaledInstance(50,50,Image.SCALE_DEFAULT);
                                _buttonArray[i][j].setIcon(new ImageIcon(img));
                            } catch (Exception ex) {
                                LOGGER.withContext("Method", "updateview")
                                        .withContext("Case", "red_king").error(ex);
                            }
                            break;
                        case BLACK:
                            try {
                                Image img = ImageIO.read(getClass().getResource("/images/black piece.jpg"));
                                img = img.getScaledInstance(50,50,Image.SCALE_DEFAULT);
                                _buttonArray[i][j].setIcon(new ImageIcon(img));
                            } catch (Exception ex) {
                                LOGGER.withContext("Method", "updateview")
                                        .withContext("Case", "black").error(ex);
                            }
                            break;
                        case BLACK_KING:
                            try {
                                Image img = ImageIO.read(getClass().getResource("/images/blackking piece.jpg"));
                                img = img.getScaledInstance(50,50,Image.SCALE_DEFAULT);
                                _buttonArray[i][j].setIcon(new ImageIcon(img));
                            } catch (Exception ex) {
                                LOGGER.withContext("Method", "updateview")
                                        .withContext("Case", "black_king").error(ex);
                            }
                            break;
                    }
                });
        _frame.pack();
    }

    /**
     * hide connect button since you can connect only once
     */
    protected void hideConnectButton()
    {
        this._connectButton.setVisible(false);
    }

    /**
     * Show connect button after disconnecting
     */
    protected void showConnectButton()
    {
        this._connectButton.setVisible(true);
    }

    /**
     * Returns whether the submit button is visible
     * @return boolean
     */
    protected boolean submitButtonIsVisible() {
        return this._submitMoveButton.isVisible();
    }

    /**
     * Hides the move buttons
     */
    protected void hideMoveButtons() {
        this._submitMoveButton.setVisible(false);
        this._clearButton.setVisible(false);
    }

    /**
     * Shows the move buttons
     */
    protected void showMoveButtons()
    {
        this._submitMoveButton.setVisible(true);
        this._clearButton.setVisible(true);
    }

    /**
     * Hides about button
     */
    protected void hideAboutButton() {
        this._aboutButton.setVisible(false);
        this._aboutButton.setVisible(false);
    }

    /**
     * Shows about button
     */
    protected void showAboutButton() {
        this._aboutButton.setVisible(true);
        this._aboutButton.setVisible(true);
    }

    /**
     * Hides the Help button
     */
    protected void hideHelpButton() {
        this._helpButton.setVisible(false);
        this._helpButton.setVisible(false);
    }

    /**
     * Shows the help button
     */
    protected void showHelpButton() {
        this._helpButton.setVisible(true);
        this._helpButton.setVisible(true);
    }

    /**
     * Changes the status of the UI at the top
     * @param status Status to be displayed
     */
    protected void changeStatus(String status)
    {
        _statusLabel.setText(status);
    }

    /**
     * Shows the message in the {@link JOptionPane}
     * @param s Message
     * @param title Title for the message
     */
    protected void showMessage(String s, String title) {
        JOptionPane.showMessageDialog(_frame, s, title, JOptionPane.PLAIN_MESSAGE);
    }

}
