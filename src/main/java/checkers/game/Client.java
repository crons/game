package checkers.game;

import checkers.engine.model.board.Board;
import checkers.engine.model.board.Piece;
import checkers.engine.model.move.Move;
import checkers.engine.model.move.Moves;
import checkers.engine.rule.MoveValidator;
import checkers.engine.rule.WinValidator;
import checkers.engine.util.LogContext;

import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.IntStream;

/**
 * <p>
 *     checkers.game.Client that opens the UI for the user to interact and play the game.
 *     It also communicates with the engine running on the server.
 * </p>
 * @author Prakash on 8/29/17.
 */
public class Client {

    private static final LogContext LOGGER = new LogContext(Client.class);

    private static View view;
    private static ObjectOutputStream out = null;
    private static ObjectInputStream in = null;
    private static Board board = null;
    private static Piece color = Piece.BLACK;
    private static ConcurrentLinkedQueue<Move> moves = new ConcurrentLinkedQueue<>();
    private static List<Point> points = new LinkedList<>();
    private static List<Color> replaceColors = new LinkedList<>(); //colors to replace for pieces

    protected final static String HELP_MESSAGE = "To make a move, click the piece you want to move followed\n"
            + "by all the positions you want to move to. These positions\n"
            + "will be highlighted on the board in green. When you are \n"
            + "done making your move press the submit button. If it is a \n "
            + "valid move the board will update and the turn will change. \n"
            + "If it is an invalid move, you will be notified that the move\n"
            + "is invalid and allowed to submit another turn. To clear your \n"
            + "move after highlighting positions press the clear buttton\n";

    protected final static String ABOUT_MESSAGE = "v1.5 Final Release\n"
            + "\nWhat's New:\n"
            + " -   fixed: missing king piece graphic\n"
            + " -   fixed: highlighting not getting removed when board is double clicked\n"
            + " -   fixed: help menu getting removed after first turn\n";

    public static void main(String[] args) {
        view = new View();
        view.hideMoveButtons();
        view.hideHelpButton();
    }

    /**
     * Threaded client that actually runs the game.
     * @return {@link Thread}
     */
    protected static Thread runGame() {
        return new Thread(
                () -> {
                    try {
                        view.hideAboutButton();
                        view.hideConnectButton();
                        view.showHelpButton();
                        Socket socket = new Socket("34.230.145.195", 8080);
                        out = new ObjectOutputStream(socket.getOutputStream());
                        in = new ObjectInputStream(socket.getInputStream());

                        boolean hasQuit = false;
                        while (!hasQuit) {
                            Object obj = in.readObject();
                            if (obj instanceof String) {
                                view.hideHelpButton();
                                String s = (String) obj;
                                if (s.equals("waitscreen")) {
                                    color = Piece.RED;
                                    view.changeStatus("Waiting for other client to connect...");
                                } else if (s.equals(("forfeit"))) {
                                    board.reset();
                                    view.updateView(board);
                                    view.showMessage("Other player forfeited", "Game Over");
                                    view.hideMoveButtons();
                                    view.changeStatus("");
                                    view.showConnectButton();
                                    view.showAboutButton();
                                    break;
                                } else if (s.equals("ping")) {
                                    out.writeObject("ping");
                                }
                            } else if (obj instanceof Board) {
                                board = (Board) obj;
                                view.showHelpButton();
                                if (WinValidator.validate(board)) {
                                    view.showMessage(board.getWinner() + " won", "Game Over");
                                    view.hideMoveButtons();
                                    view.changeStatus("");
                                    view.showConnectButton();
                                    view.showAboutButton();
                                    board.reset();
                                    break;
                                }
                                view.updateView(board);
                                setStatus(color, board.getTurn());
                                if (color == board.getTurn()) {
                                    while (true) {
                                        if (!view.submitButtonIsVisible()) {
                                            view.showMoveButtons();
                                        }
                                        if (moves.size() != 0) {
                                            Moves moves = getMoves();
                                            if (!MoveValidator.validate(moves, board)) {
                                                view.showMessage("Invalid move", "Move Error");
                                                clearMoveSequence();
                                            } else {
                                                out.writeObject(moves);
                                                clearMoveSequence();
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    if (view.submitButtonIsVisible()) {
                                        view.hideMoveButtons();
                                    }
                                    out.writeObject("ping");
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOGGER.withContext("Method", "runGame")
                                .error(e);
                        e.printStackTrace();
                        view.changeStatus("Cannot connect to game at this time");
                        view.showConnectButton();
                    }
        });
    }

    /**
     * Sets the status based on the {@link Piece} of color and turn
     * @param color Color for the {@link Piece}
     * @param turn Turn for the {@link Piece}
     */
    protected static void setStatus(Piece color, Piece turn) {
        String playerColor = color.isBlack() ? "black" : "red";
        String turnColor = turn.isBlack() ? "black" : "red";
        view.changeStatus(String.format("You color: %s     Current turn: %s %n", playerColor, turnColor));
    }

    /**
     * Starts the game
     */
    protected static void connectButtonClicked()
    {
        runGame().start();
    }

    /**
     * Functionality of the submit button. (Used to send the moves to the server)
     */
    protected static void submitButtonClicked() {
        //create list of Moves using the list of points
        //send the list of Moves to server
        if (points.size() < 2) {
            view.showMessage("Invalid Move", "Move Error");
            clearMoveSequence();
        } else {
            //convert points to moves
            IntStream.range(0, points.size() - 1)
                    .forEach( i -> {
                        int x1 = points.get(i).x;
                        int y1 = points.get(i).y;
                        int x2 = points.get(i+1).x;
                        int y2 = points.get(i+1).y;
                        moves.add(new Move(x1, y1, x2, y2));
                    });
        }
    }

    /**
     * Clears the {@link Moves} linked to the {@link Board}
     */
    private static void clearMoveSequence() {
        for (int i = replaceColors.size()-1; i >= 0; i--) {
            Point p = points.get(i);
            view.setPositionBGColor(p, replaceColors.get(i));
        }
        replaceColors.clear();
        moves.clear();
        points.clear();
    }

    /**
     * Shows a message when the help button is clicked
     */
    protected static void helpButtonClicked() {
        view.showMessage(HELP_MESSAGE, "Help");
    }

    /**
     * Shows the message when the about button is clicked
     */
    protected static void aboutButtonClicked() {
        view.showMessage(ABOUT_MESSAGE, "About");
    }

    /**
     * Functionality of the clear button. (Clears the move sequence)
     */
    protected static void clearButtonClicked() {
        clearMoveSequence();
    }

    /**
     * Functionality of the Game button
     * @param point {@link Point}
     */
    protected static void gameButtonClicked(Point point)
    {
        if (board.getTurn() == color) {
            points.add(point);
            replaceColors.add(view.getPositionBGColor(point));
            view.setPositionBGColor(point, Color.GREEN);
        }
    }

    /**
     * Gets the {@link Moves} required to send to the server for validation
     * @return {@link Moves}
     */
    protected static Moves getMoves() {
        List<Move> moveList = new LinkedList<>();
        moveList.addAll(moves);
        return new Moves(moveList.toArray(new Move[moveList.size()]));
    }

    /**
     * Sends forfeit to the server for cutting off the connection.
     */
    protected static void sendForfeit() {
        try {
            if (out != null)
                out.writeObject("forfeit");
        }
        catch (IOException e) {
            LOGGER.withContext("Method", "sendForfeit")
                    .error(e);
            e.printStackTrace();
        }
    }


}
