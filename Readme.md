# Release Notes

#### v1.0.RELEASE Final Release
##### What's new:
* fixed: added a gradle wrapper for cross-platfor installation
* fixed: added the findbugs and javadoc integration
* fixed: Added a depency management for the engine
* fixed: Added automated deployment pipeline build environment.
